const presets = [
  ['@babel/env', {
    targets: ['ie >= 0', 'op_mini >= 0'],
    useBuiltIns: 'usage'
  }]
]

module.exports = { presets }
