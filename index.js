const SELF_CLOSING_TAGS = [
  'area',
  'base',
  'br',
  'col',
  'command',
  'embed',
  'hr',
  'img',
  'input',
  'keygen',
  'link',
  'meta',
  'param',
  'source',
  'track',
  'wbr'
]
const isSelfClosing = tag => SELF_CLOSING_TAGS.indexOf(tag) > -1

const is = value => value
const isString = value => typeof value === 'string'
const isNumber = value => !isNaN(value) && typeof value === 'number'
const isAnyObject = value => value === Object(value)
const isValid = value => isString(value) || isNumber(value) || isAnyObject(value)

const isArray = value => Array.isArray(value)
const isFunction = value => value && {}.toString.call(value) === '[object Function]'
const isObject = value => !Array.isArray(value) &&
  !isFunction(value) &&
  isAnyObject(value)

const buildProperties = properties => properties
  .map(entry => buildProperty(entry))
  .filter(is)
  .join(' ')

const buildTag = element => {
  element.tag = `<${element.data.tag}`

  // Attach the the data properties as HTML attributes.
  if (element.data.className) {
    element.data.class = element.data.className
  }

  const filter = property => entry => entry[0] !== property

  element.properties = Object.entries(element.data)
    .filter(filter('tag'))
    .filter(filter('className'))

  element.properties = element.selfClosing
    ? element.properties
    : element.properties.filter(filter('child'))

  if (element.properties.length) {
    const properties = buildProperties(element.properties)

    if (properties.length) {
      element.tag += ` ${properties}`
    }
  }

  element.tag += '>'

  return element.tag
}

const formatProperty = (name, property) => `${name}="${property}"`

const parse = (value, delimiter = '') => {
  if (isNumber(value)) {
    return value.toString()
  } else if (isFunction(value)) {
    return build(value(), { delimiter })
  } else if (isString(value)) {
    return value
  } else if (isArray(value)) {
    const result = value.filter(isValid).map(build).filter(isValid).join(delimiter)

    return result
  } else {
    return null
  }
}

const buildProperty = entry => {
  const name = entry[0]
  const value = entry[1]

  if (value === '' || value === true) {
    return name
  } else {
    const parsed = parse(value, ' ')
    if (parsed) {
      return formatProperty(name, parsed.toString())
    } else if (isObject(value)) {
      const entries = Object
        .entries(value)
        .map(entry => [`${name}-${entry[0]}`, entry[1]])

      return buildProperties(entries)
    } else {
      return null
    }
  }
}

const build = (data, options) => {
  const delimiter = options && options.delimiter ? options.delimiter : ''

  const result = parse(data, delimiter)

  if (result) {
    return result
  } else if (isObject(data)) {
    if (!data.tag) {
      data.tag = 'div'
    }

    const element = { data }
    element.selfClosing = isSelfClosing(data.tag)
    let output = buildTag(element)

    if (!element.selfClosing) {
      if (data.child) {
        output += build(data.child)
      }

      output += `</${data.tag}>`
    }

    return output
  }
}

module.exports = build
