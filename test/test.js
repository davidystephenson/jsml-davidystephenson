/* global describe, it */

// TODO document engine, include
// TODO Modularize engine?

const assert = require('assert')
const jsml = require('../index')

;(() => {
  describe('JavaScript Markup Language', () => {
    describe('build', () => {
      describe('strings', () => {
        it('returns a string when passed a populated string', () => {
          const text = 'hello world'
          const output = jsml(text)

          assert.strictEqual(text, output)
        })
      })

      describe('numbers', () => {
        it('return a string when passed a number', () => {
          const number = 1
          const output = jsml(number)

          const html = '1'
          assert.strictEqual(output, html)

          const number2 = 8675309
          const output2 = '8675309'
          assert.strictEqual(jsml(number2), output2)
        })
      })

      describe('functions', () => {
        it('use the output of a passed function', () => {
          const element = () => ({
            child: [
              'hello world!',
              () => ({ tag: 'span', child: 'how are you?' })
            ]
          })
          const output = jsml(element)

          const html = `<div>hello world!<span>how are you?</span></div>`
          assert.strictEqual(output, html)

          const fn = () => 2 + 2
          assert.strictEqual(jsml(fn), '4')
        })
      })

      describe('objects', () => {
        it('return a div element when passed an empty object', () => {
          const element = {}
          const output = jsml(element)

          const html = '<div></div>'
          assert.strictEqual(output, html)
        })

        it('return a populated div when passed only a child property', () => {
          const element = { child: 'text' }
          const output = jsml(element)

          const html = `<div>text</div>`
          assert.strictEqual(output, html)
        })

        it('return a element with any tag when passed a tag property', () => {
          const element = { tag: 'svg' }
          const output = jsml(element)

          const html = '<svg></svg>'
          assert.strictEqual(output, html)

          assert.strictEqual(
            jsml({ tag: 'p' }),
            '<p></p>'
          )
        })

        describe('element attributes', () => {
          it('return an element with attributes when passed an object with properties', () => {
            const element = { tag: 'button', class: 'button', type: 'button', child: 'Click Me!' }
            const output = jsml(element)

            const html = `<button class="button" type="button">Click Me!</button>`
            assert.strictEqual(output, html)
          })

          it('with array values are collapsed into strings', () => {
            const element = { tag: 'button', class: ['btn', 'btn-default'] }
            const output = jsml(element)

            const html = '<button class="btn btn-default"></button>'
            assert.strictEqual(output, html)
          })

          describe('with object values', () => {
            it('are expanded with dashes if populated', () => {
              const element = { data: { one: 'a', two: 'b' }, aria: { hidden: 'true' } }
              const output = jsml(element)

              const html = '<div data-one="a" data-two="b" aria-hidden="true"></div>'
              assert.strictEqual(output, html)
            })

            it('ignores invalid sub-properties', () => {
              const element = { aria: { hidden: 'true', invalid: null, empty: {} } }
              const output = jsml(element)

              const html = '<div aria-hidden="true"></div>'
              assert.strictEqual(output, html)
            })

            it('expands recursively', () => {
              const element = {
                x: {
                  array: ['something', 'else'],
                  y: {
                    a: 1,
                    b: 2
                  },
                  z: {
                    c: {
                      d: 3,
                      invalid: false
                    }
                  }
                }
              }
              const output = jsml(element)

              const html = '<div x-array="something else" x-y-a="1" x-y-b="2" x-z-c-d="3"></div>'
              assert.strictEqual(output, html)
            })
          })

          it('do not provide an attribute value when given an empty string', () => {
            const element = { tag: 'button', disabled: '' }
            const output = jsml(element)

            const html = '<button disabled></button>'
            assert.strictEqual(output, html)
          })

          it('do not provide an attribute value when given the boolean true', () => {
            const option = { tag: 'option', disabled: true, selected: '' }
            const output = jsml(option)

            const html = '<option disabled selected></option>'
            assert.strictEqual(output, html)
          })

          describe('with invalid values', () => {
            it('are ignored when there is a valid attribute', () => {
              const element = {
                tag: 'option',
                layout: null,
                horizontal: {},
                something: false,
                disabled: true,
                else: 'true',
                child: 'something'
              }
              const output = jsml(element)

              const html = '<option disabled else="true">something</option>'
              assert.strictEqual(output, html)
            })

            it('are ignored when there is no valid value', () => {
              const div = jsml({ else: false, prop: {} })
              assert.strictEqual(div, '<div></div>')
            })
          })

          it(
            'return an element with a class attribute matching the className property when passed on a className property, even when a class property is provided.',
            () => {
              const element = { class: 'active', className: 'inactive' }
              const output = jsml(element)

              const html = '<div class="inactive"></div>'
              assert.strictEqual(output, html)

              assert.strictEqual(
                jsml({ class: 'hero', child: 'Welcome!' }),
                '<div class="hero">Welcome!</div>'
              )

              assert.strictEqual(
                jsml({ tag: 'img', className: 'icon' }),
                '<img class="icon">'
              )

              assert.strictEqual(
                jsml({ class: 'inactive', className: 'active' }),
                '<div class="active"></div>'
              )
            }
          )

          it('return an element with multiple classes when passed a class array property', () => {
            const element = { className: ['tab', 'active'] }
            const output = jsml(element)

            const html = '<div class="tab active"></div>'
            assert.strictEqual(output, html)
          })

          it('use the output of methods', () => {
            const element = { className () { return ['tab', 'inactive'] } }
            const output = jsml(element)

            const html = '<div class="tab inactive"></div>'
            assert.strictEqual(output, html)
          })
        })

        it('return a element with a child attribute when passed a self-closing tag', () => {
          const element = {
            tag: 'meta',
            name: 'viewport',
            content: ['width=device-width,', 'initial-scale=1'],
            child: 'unnecessary'
          }
          const output = jsml(element)

          const html = '<meta name="viewport" content="width=device-width, initial-scale=1" child="unnecessary">'
          assert.strictEqual(output, html)

          assert.strictEqual(jsml({ tag: 'img', child: '#framework-id' }), '<img child="#framework-id">')
        })

        it('return an element with text child when passed an object with a text child property.', () => {
          const element = { tag: 'p', child: 'text' }
          const output = jsml(element)

          const html = `<p>text</p>`
          assert.strictEqual(output, html)
        })

        it('return an element with a child when passed an object with object child.', () => {
          const element = { tag: 'span', class: 'parent', child: { class: 'child' } }
          const output = jsml(element)

          const html = `<span class="parent"><div class="child"></div></span>`
          assert.strictEqual(output, html)
        })

        it('return an element with nested children when passed an object with nested child', () => {
          const element = {
            class: 'container',
            child: [
              {
                tag: 'button',
                type: 'button',
                class: 'btn btn-default',
                child: [
                  'Click Me!',
                  { tag: 'img', src: '/public/safe.png' }
                ]
              },
              "DON'T CLICK THAT BUTTON!!!",
              () => ({ tag: 'p', child: "It's not safe!" }),
              { tag: 'input', value: 1 }
            ]
          }
          const output = jsml(element)

          const html = `<div class="container"><button type="button" class="btn btn-default">Click Me!<img src="/public/safe.png"></button>DON'T CLICK THAT BUTTON!!!<p>It's not safe!</p><input value="1"></div>`
          assert.strictEqual(output, html)

          const button = jsml({
            tag: 'button',
            type: 'button',
            class: ['btn', 'btn-default', 2 + 2],
            child: () => 'Click Me!'
          })
          assert.strictEqual(
            button,
            '<button type="button" class="btn btn-default 4">Click Me!</button>'
          )

          assert.strictEqual(
            jsml({
              class: 'container',
              child: [
                { tag: 'h1', child: 'Call to Action' },

                'A summarizing description',

                { tag: 'img', src: '/public/logo.png' },

                button
              ]
            }),
            '<div class="container"><h1>Call to Action</h1>A summarizing description<img src="/public/logo.png"><button type="button" class="btn btn-default 4">Click Me!</button></div>'
          )
        })
      })

      describe('arrays', () => {
        it('return a series of elements when passed an array', () => {
          const elements = [{ child: 'first' }, { tag: 'img', src: 'http://google.com/logo.png' }, [' 3 ', 4]]
          const output = jsml(elements)

          const html = `<div>first</div><img src="http://google.com/logo.png"> 3 4`
          assert.strictEqual(output, html)

          assert.strictEqual(
            jsml([1, ' ', 2, ' 3 ', 4]),
            '1 2 3 4'
          )

          assert.strictEqual(
            jsml([
              'first ',
              () => 2,
              [' third ', 4]
            ]),
            'first 2 third 4'
          )
        })

        it('ignore invalid elements', () => {
          const elements = [
            'first ',
            false,
            'second ',
            null,
            'third ',
            4
          ]
          const output = jsml(elements)

          const html = 'first second third 4'
          assert.strictEqual(output, html)

          assert.strictEqual(
            jsml([
              'first ',
              false,
              () => 2,
              null,
              ' third'
            ]),
            'first 2 third'
          )
        })
      })
    })
  })
})()
